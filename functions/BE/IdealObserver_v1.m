% IdealObserver: Converts the overall SNRenv to percent correct.
% 
% Usage: [Pcorrect SNRenv ] = IdealObserver(SNRenv_lin,parameters)
% 
% SNRenv_lin :  vector with the SNRenv values (not in dB) for each input SNR             
% Parameters :  vector with the parameters for the ideal Observer formatted as [k q m sigma_s] 
%
% S�ren J�rgensen august 2010
% last update 10-June 2013
% Copyright S�ren J�rgensen

function Pcorrect   = IdealObserver_v1(SNRenv_lin,parameters)

if nargin < 2
    error('You have to specify the k,q,m,sigma_s parameters for the IdealObserver')
end
k = parameters(1);
q = parameters(2);
m = parameters(3);

sigma_s = parameters(4);


% ---------- Converting from SNRenv to d_prime  --------------
    d_prime = k*(SNRenv_lin).^q; 

%----------- Converting from d_prime to Percent correct, Green and Birdsall (1964)----------
    Un = 1*norminv(1-(1/m)); 
    mn = Un + (.577 /Un);% F^(-1)[1/n] Basically gives the value that would be drawn from a normal destribution with probability p = 1/n.
    sig_n=  1.28255/Un; 
    Pcorrect = normcdf(d_prime,mn,sqrt(sigma_s.^2+sig_n.^2))*100;  

        
% Green, D. M. and Birdsall, T. G. (1964). "The effect of vocabulary size", 
% In Signal Detection and Recognition by Human Observers, 
% edited by John A. Swets (John Wiley & Sons, New York), 609-619.
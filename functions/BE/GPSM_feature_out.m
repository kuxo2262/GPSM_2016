function [results] = GPSM_feature_out(signal,ref,work,simwork,dc2mod,def)
 % GPSM_feature_out.m This function calculates based on envelope power and power SNRs 
%                      the final combined SNR from which speech reception
%                      thresholds are estimated.
% 
% 
% INPUT:
%                     signal        ...   signal + noise
%                        ref        ...   noise
%                       work        ...   some side-calculations on
%                                         signal+noise
%                    simwork        ...   struct for parameters applied during
%                                         simulation, e.g., filter coefficients
%                     dc2mod        ...   3-dim output matrix containing
%                                         power-based values to weight envelope
%                                         power SNRs
%                       def         ...   struct for various parameters, e.g.,fs
% OUTPUT:
%          results.SNR_comb         ...   SNR-value from combining
%                                         envelope-power- and power-based SNRs
%          results.SNR_ac           ...   Envelope-power-based SNRs
%          results.SNR_dc           ...   Power-based SNRs
%          results.SNR_comb_dB      ...   Combined SNR-value in dB
%
% Usage: [results] = GPSM_feature_out(signal,ref,work,work_ref,simwork,dc2mod)
% author: thomas.biberger@uni-oldenburg
% date:   2016-10-09
% update: 2018-12-11, 2021-05-25


%% set some restrictions/limitations
ref=min(signal,ref);
ref(:,2:end)=max(ref(:,2:end),0.002);
signal(:,2:end)=max(signal(:,2:end),0.002);

%% SNR calculation
SNR_temp_mod_per=(signal-ref)./ref; % SNR over auditory and modulation channels
dc2mod=max(dc2mod,0);
SNR_temp_mod_per=SNR_temp_mod_per.*dc2mod;

%% only enelope power SNRs above hearing threshold are considered (only relevant in near-threshold situations)
validate_ref=ref(:,1);
validate_sig=signal(:,1);
signal_temp=SNR_temp_mod_per';
signal_temp(2:end,validate_ref(1,:)<=1e-10 | validate_sig(1,:)<=1e-10)=0; %tb

%% only modulation filter center frequencies up to one fourth of the corresponding auditory channel center freq. are considered (Verhey et al.,1999)

if strcmp(def.predictions,'SI')==1;
ModFiltersMatrix = [[1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;]';
ModFiltersMatrix=[ones(1,22);ModFiltersMatrix];
modFiltMat=ones(10,22);


modFiltMat(ModFiltersMatrix==0)=0;
signal_temp_jeskLim=signal_temp.*modFiltMat;

signal_temp=signal_temp_jeskLim.^2;

%% combining SNRs across auditory/modulation channels
% here envelope power and power SNRs are regarded seperately as proposed in Biberger & Ewert (2016)
% combining dc-channels
signal_temp_dc=signal_temp(1,:);
SNR_dc=sqrt(sum(signal_temp_dc,2));

% combining ac-channels
signal_temp_ac=signal_temp(2:end,:);
SNR_periph_ac=sum(signal_temp_ac,2);  % original tb
SNR_ac=sqrt(sum(SNR_periph_ac,1));    % original tb

% using DC just as another row
signal_temp_all=signal_temp(1:end,:);
SNR_periph_all=sum(signal_temp_all,2);
SNR=sqrt(sum(SNR_periph_all,1));

elseif strcmp(def.predictions,'PSY')==1;
ModFiltersMatrix = [[1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:5 0 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0]; [1:6 0 0 0];...
    [1:7 0 0] ; [1:7 0 0]; [1:7 0 0]; [1:8 0 ]; [1:8 0 ]; [1:8 0 ]; 1:9; 1:9; 1:9; 1:9;...
    1:9; 1:9; 1:9; 1:9; 1:9; 1:9;1:9;1:9]';
ModFiltersMatrix=[ones(1,24);ModFiltersMatrix];
modFiltMat=ones(10,24);

modFiltMat(ModFiltersMatrix==0)=0;
signal_temp_jeskLim=signal_temp.*modFiltMat;

signal_temp=signal_temp_jeskLim.^2;

%% combining SNRs across auditory/modulation channels
% here envelope power and power SNRs are regarded seperately as proposed in Biberger & Ewert (2016)
% combining dc-channels
signal_temp_dc=signal_temp(1,:);
SNR_dc=sqrt(sum(signal_temp_dc,2));

% combining ac-channels
signal_temp_ac=signal_temp(2:end,:);
SNR_periph_ac=sum(signal_temp_ac,2);  % original tb
SNR_ac=sqrt(sum(SNR_periph_ac,1));    % original tb

SNR=(SNR_ac*0.21+SNR_dc*0.45);

else
     error('Invalid value of def.predictions!')
end
    
%% output
results.SNR_comb=SNR;
results.SNR_ac=SNR_ac;
results.SNR_dc=SNR_dc;
results.SNR_comb_dB=10*log10(SNR);
end
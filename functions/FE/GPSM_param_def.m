function [def simdef simwork] = GPSM_param_def(def)
% GPSM_param_def.m Sets some parameters of model stages and preallocates
% some values/outputs to be calculated only one time (e.g. auditory filterbank)
% 
% INPUT:    
%           def     ...  struct vor various parameters, e.g., fs
% 
% 
% OUTPUT:   
%           def     ...  struct for stimulus related parameters, e.g., fs
%           simdef  ...  struct for model related parameters, e.g., filter
%                        order, cut-off frequency
%           simwork ...  struct for parameters applied during simulation,
%                        e.g., filter coefficients 
% 
% 
% Usage: [def simdef simwork] = GPSM_param_def(def)
% authour: thomas.biberger@uni-oldenburg.de
% update: 2019-02-13, 2021-05-21


%% signal preprocessing/preallocation

[simdef]=GPSM_model_def();     % set parameters of model stages (e.g. spacing of auditory filters, filter order)
[def simdef simwork]= GPSM_init(def,simdef);          % preallocates some values/outputs that must be computed one time only (e.g. auditory filterbank)

end


function[out work dc2mod]=ACDC_lt(input,gtfb_num,mfb_num,fs,work,signal_rect);
% ACDC_lt.m This function calculates the long-term envelope power and power
%
%   INPUT:
%       input           ...  3-dim. input matrix representing the input signal
%                       ...  input(time,audio channels,intensity/modulation channels)
%       gtfb_num        ...  number of auditory channels
%       mfb_num         ...  number of modulation channel + 1 intensity
%                            channel
%       fs              ...  sampling frequency
%       work            ...  some side-calculation on the input signal
%       signal_rect     ...  low-pass filtered (150 Hz) hilbert envelope per auditory
%                       ...  channel (see mrGPSM_preproc.m)
% 
%
%   OUTPUT:
%       out             ...  3-dim. output matrix containing multi-res. envelope
%                            power and power of the corresponding auditory and modulation
%                            filter
%       work            ...  some side informations (e.g. number of auditory
%                            channels), but also some additional signals which
%                            might be useful for analyzation
%       dc2mod          ...  3-dim output matrix containing power-based values
%                            to weight envelope power SNRs
%
% Usage:  [out work dc2mod]=ACDC_lt(input,gtfb_num,mfb_num,fs,work,signal_rect)
% author: thomas.biberger@uni-oldenburg
% date:     2016-10-03
% update:   2018-03-18, 2021-05-20

env_pow_lim=10^-2.7;      % envelope power limit, default -30dB related to 100% modulated SAM

upper_lim=10^(-6.5); % before -65 dB
lower_lim=10^(-10);  % -100 dB

slope=1/((10*log10(upper_lim)+100)-(10*log10(lower_lim)+100));  % +100 for trafo from -100...0dB to 0...100 dB

nWin=zeros(length(gtfb_num),length(mfb_num));
winSamp=zeros(length(gtfb_num),length(mfb_num));
out=zeros(length(gtfb_num),length(mfb_num));
dc2mod=ones(length(gtfb_num),length(mfb_num));

%% Mult-Resolution Processing
for ii = 1:length(gtfb_num); % gtfb loop
    for kk= 1:length(mfb_num);   % mfb loop
        
         % START of calculating the global DC-power 
        tmp=squeeze(input(:,ii,kk));
        
        
        if mfb_num(kk)==0       % for dc part
            tmp_pow=mean(signal_rect(ii,:));
             tmp_pow=(tmp_pow^2);
             
            if tmp_pow<=1e-10;
                tmp_pow=1e-10;
            else
            end
            
            out(ii,1)=tmp_pow;           
            nWin(ii,1)=1;
            winSamp(ii,1)=length(tmp);
            
            dc2mod(ii,1)=1;
          
            
        else                  % here starts the envelope power calcuation
                     
                tmp=tmp*sqrt(2);
                             
                    if tmp_pow<=1e-10;
                        tmp_pow_ac=env_pow_lim;  % squeeze out vorher allokieren                        
                    else
                        tmp_pow_ac=var(tmp)/tmp_pow;  % squeeze out vorher allokieren               
                    end    
               
                if out(ii,1)> upper_lim;  % here comes the dc2mod-sensitivity trafo for dc-levels below upper_lim   
                    dc2mod(ii,kk)=1;
                else
                    dc2mod(ii,kk)=slope*(10*log10(out(ii,1))+100);
                end
                
               out(ii,kk)=tmp_pow_ac;
        end 
    end
work.nWin=nWin;
work.winSamp=winSamp;
end

function [output work dc2mod] = mreGPSM_preproc(x,def,simdef,simwork)
% GPSM_preproc.m This function includes the front end processing stages
%                    to calculate power and envelope power features
% 
%   Processing stages of GPSM
%   1. auditory filtering by gammatone filterbank
%   2. envolope extraction by hilbert trafo
%   3. downsampling (optional)
%   5. modulation filterbank
%   6. envelope power & power calculation 
%
%   INPUT:
%       x              ...   input signal
%       def            ...   struct for stimulus related parameters,e.g.,fs
%       simdef         ...   struct for model related parameters, e.g., filter order,
%                            cut-off frequency 
%       simwork        ...   struct for parameters applied during simulation, e.g.,
%                            filter coefficients
%
%   OUTPUT:
%       Y          ...   3-dim. output matrix containing multi-res. envelope
%                            power of the corresponding auditory and modulation
%                            filter
%       work       ...   some side informations (e.g. number of auditory
%                            channels), but also some additional signals which
%                            might be useful for analyzation
%       dc2mod     ...   3-dim output matrix containing power-based values
%                            to weight envelope power SNRs 
%                       
%
% Usage: [out] = GPSM_preproc(x,def,simdef,simwork)
% author: thomas.biberger@uni-oldenburg
% date:   2016-11-18
% modified: 2018-11-28, 2021-05-20

%% ISO-hearing threshold
  [signal_me]=isothr_input(x,def.samplerate,simwork);
  signal_me=signal_me';

%% Auditory Filtering (GTFB (with complex-valued output) adopted from Dietz2011 )
simwork.analyzer=gfb_analyzer_clear_state(simwork.analyzer);
channels = length(simwork.analyzer.center_frequencies_hz);
fc=simwork.analyzer.center_frequencies_hz;
% apply auditory filterbank to the outer&middle-ear-filtered 
[signal_filtered, simwork.analyzer] = gfb_analyzer_process(simwork.analyzer, signal_me(:,1));

%% Hilbert Envelope
signal_rect=abs(signal_filtered')/sqrt(2);  % hilbert envelope divided thru sqrt(2) cause power of the hilbert envelope is increased by factor of 2

%% Lowpass 150 HZ 
signal_rect= filter(simwork.b_env_lp,simwork.a_env_lp,signal_rect);

%% Resampling (default downsampling by factor 2)
if strcmp(simdef.resampling,'on')==1;
       signal_rect=resample(signal_rect,1,def.downsample_factor);
       signal_rect=signal_rect';
       work.signal_rect_down=signal_rect;

elseif strcmp(simdef.resampling,'off')==1;
      % no downsampling
      signal_rect=signal_rect';
      work.signal_rect_down=signal_rect;

else
    disp('Please define if resampling should be used!')
end

%% Modulation Filterbank (AM Processing)
    Y= zeros(length(signal_rect(1,:)),channels,20);
    for ii =1:channels; 
         [inf_1,y]= mfb2_GPSM(signal_rect(ii,:),simdef.mf_mfb2style,def.samplerate_down);
         Y(:,ii,1:length(inf_1)+1)=[y(:,1),y];
     end
    
    Y=Y(:,:,1:length(inf_1)+1);
    work.inf_1=[inf_1(1) 1 inf_1(2:length(inf_1))];
        
%% Calculation of envelope power using multi-resolution analysis and long-time power
    [output work dc2mod]=ACst_DClt(Y,fc,work.inf_1,def.samplerate_down,work,signal_rect);

end
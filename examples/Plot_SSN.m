% Plot data from mrGPSM and empirical data. This function is a slightly modified function
% of S�ren J�rgensen's function 'Plot_SSN.m'.
clc,clear all,close all

% define the number of sentences that were used
NSpeechsamples = 20;

% load the .mat result file for SSN maskers
load mrGPSM_female_masker_female_20sent_21-02-2019_SSN_1
SNRs=result.SNRs;

% user has typically adjust parameters 1 & 2
IOparameters = [0.45 0.5 50 0.6]; % parameters from J�rgensen et al., (2013).
% first parameter represents the constant k to calculate d' from the SNR
% second parameter reprents the constant q to calculate d' from the SNR
% third parameter represents the response-size set (Closed OLSA test: 50)
% fourth parameter represents: sigma s (related to the redundancy of the speech material)

for q = 1:NSpeechsamples
    for k = 1:length(SNRs)
        SNR(k,q) = result.res{k,q}.SNR_comb;
    end
    Pc_est(:,q)  = IdealObserver_v1(SNR(:,q),IOparameters);     
end

% Average across speech samples
Pc_est_mean_SSN = mean(Pc_est,2);
SNR_mean_SSN = mean(SNR,2);

%******************************************************
%******************************************************
%******************************************************

    fnts = 14;
    fig=figure;
    sh = 350;
    set(fig,'Position',[1.5*sh, 1.4*sh, 2.5*sh, 1*sh]);
    subplot(1,2,1)
    plot(SNRs,Pc_est_mean_SSN ,'-- o','color','k', 'markersize', 12),hold on   
    data_CLUE = [50];% measured SRT50 Schubotz et al. (2016)
    plot([-7.66],data_CLUE,'s','color','k','markerfacecolor','k','markersize', 15) 
    xlabel('SNRs (dB)','FontSize',fnts);
    ylabel('Percent correct %','FontSize',fnts);
    ylim([0 100])
    xlim([SNRs(1) 4])
    
    set(gca,'FontSize',fnts,'xtick',SNRs);
    lgnLables = {'mrGPSM','Schubotz et al. (2016)'};
    
    le =legend(lgnLables);
    set(le,'box','off','location','southeast','fontsize',12)
    
    subplot(1,2,2)
    plot(SNRs,10*log10(SNR_mean_SSN),' -o','markersize',12,'color','k'), hold on
    xlabel('SNRs (dB)','FontSize',fnts);
    ylabel('combined SNR (dB)','FontSize',fnts);
    ylim([0 20])
    xlim([SNRs(1) 4])
   
    set(gca,'FontSize',fnts,'xtick',SNRs);
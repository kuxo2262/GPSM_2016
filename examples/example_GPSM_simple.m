clc
clear all
close all;

% select if you want to predict speech intelligibility or psychoacoustics
% 'SI' corresponds to speech intelligibility
% 'PSY' corresponds to psychoacoustics
def.predictions='SI'; 
%% this is a simple example how to apply the model for speech intellgiblity predictions

% set the sampling frequency used by the model (e.g., 22050 Hz)
fs=22050;
 
% read in masker signal
if verLessThan('matlab','8.0')
    [noise fs_mask]=wavread(['SSN_longtermspec_ISTS.wav']);
else
    [noise fs_mask]=audioread(['SSN_longtermspec_ISTS.wav']);
end

% read in target signal
if verLessThan('matlab','8.0')
    [x fs_target]=wavread(['06044.wav']);
else
    [x fs_target]=audioread(['06044.wav']);
end

x=x(:,1);

if fs_target ~= fs_mask,
    error('signals have different sampling frequencies')
else
    fs_sig=fs_target;
end

% downsampling of target/masker signal to the required sampling freq. of the model  (here factor 2)
ds_fact=fs_sig/fs;
[noise]=resample(noise,1,ds_fact); % downsampling from 44100 Hz to 22050 Hz %
[x]=resample(x,1,ds_fact); % downsampling from 44100 Hz to 22050 Hz

% Compute the length of the target signal
Ts = 1/fs;
T = length(x)/fs;
t = 0:Ts:T-Ts;
N = length(t);

% select a noise snippet with the same length as the speech sentence
Nsegments = floor(length(noise)/N);
startIdx = 1; 
noise = noise(startIdx:startIdx+N -1);

% define the noise presentation level
SPL = 65; % noise presentation level
noise = (noise/rms(noise))*10^(((SPL-100))/20); 
noise_orig=noise;

% define the SNR
SNRs = [-15];  

% define the sentence presentation level
sentenceFileLevel = -24.00;
x_cal=x.*10^(((SPL-sentenceFileLevel+SNRs-100))/20); % OLSA calib tb
if size(noise) ~= size(x)
    noise = 'noise';
end
noise=noise_orig; % only noise
test = noise + x_cal; % mixture of speech + noise

%% here starts the GPSM (monaural)
[tmp] = GPSM(test,noise,fs,def);

%% Model output
disp(['combined SNR: ',num2str(tmp.SNR_comb)])
disp(['envelope power SNR: ',num2str(tmp.SNR_ac)])
disp(['power SNR: ',num2str(tmp.SNR_dc)])
% Values below represent model outputs for default input signals:
%                         tmp.SNR_comb:      3.8596
%                         tmp.SNR_ac:        1.832
%                         tmp.SNR_dc:        3.3971

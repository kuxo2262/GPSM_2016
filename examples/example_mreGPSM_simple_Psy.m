clc
clear all
close all;


% select if you want to predict speech intelligibility or psychoacoustics
% 'SI' corresponds to speech intelligibility
% 'PSY' corresponds to psychoacoustics
def.predictions='PSY'; 
%% this is a simple pure tone intensity JND example to show how to apply the model for psychoacoustic predictions
% reference signal: 1 kHZ, 60 dB SPL
% test signal: 1 kHz, 61 dB SPL

ref_lev=60; % reference level of 60 dB SPL;
test_lev=61; % test level of 61 dB SPL; (as the model only accounts for increments, 
             % the test level has to be larger than reference levels. otherwise signal is not detected )

% set the sampling frequency used by the model (e.g., 22050 Hz)
fs=44100;
sig_dur=1; % signal duration 1 sec
flank_dur=0.125; % duration of hanning flank
t=0:1/fs: sig_dur-1/fs;

% hann window
hann_flanks=hann((2*flank_dur)*fs);
refWin=[hann_flanks(1:ceil(length(hann_flanks)/2));ones((sig_dur-(2*flank_dur))*fs,1);hann_flanks(ceil(length(hann_flanks)/2)+1:end)];

% 1kHz pure tone
reference = sin(2*pi*1e3*t)'.* refWin;

% get the correct levels (variable level of signal from experiment variable)
ref_sig = reference/rms(reference) * 10^((ref_lev-100)/20);
test_sig = reference/rms(reference) * 10^((test_lev-100)/20);

%% here starts the mreGPSM (monaural)
[tmp] = mreGPSM(test_sig,ref_sig,fs,def);

% take the maximum value, either from envelope power or power SNRs
SNR_add=(tmp.SNR_ac*0.21+tmp.SNR_dc*0.45); % as described in Biberger & Ewert (2016)
% SNR into d'
d_prime=sqrt(2*SNR_add);

if d_prime >sqrt(0.5);  % default, as it is in the paper
    disp(['Signal detected'])
else
    disp(['Signal not detected'])
end

%% Model output
disp(['dprime: ',num2str(d_prime)])
disp(['envelope power SNR: ',num2str(tmp.SNR_ac)])
disp(['power SNR: ',num2str(tmp.SNR_dc)])

%                         dprime:               0.7852
%                         tmp.SNR_ac:           5.6859-12
%                         tmp.SNR_dc:           0.68505
function out=ccf(in1,in2)
%CCF cyclic convolution function
%     CCF(X,Y) is the cyclic cross-correlation of input vectors X and Y.
%     The cccf is calculated via FFT. Both input vectors must be of the
%     same size.
if size(in1,1) ~= size(in2,1)
   return
end   
%len=size(in1,1);
%----------------direct computation of cross-correlation------------
%out=zeros(len,1);
%for I=1:len,
%   I2=I-2
%   for J=1:len,
%      out(I,1)=in1(J,1)*in2(mod(J+I2,len)+1,1)+out(I,1);
%   end
%end
%-------------------------------------------------------------------
%zero=zeros(len,1);
%cin1=[in1;in1];
%cin2=[zero;in2];
%out=xcorr(cin1,cin2,len-1);
%out=out(len:2*len-1);
%
%------------------just the best way--------------------------------



%% zero padding to 
% in1_zp=[in1; zeros(length(in2)-1,1)];
% in2_zp=[in2; zeros(length(in1)-1,1)];


add_in2=length(in1)-length(in2);
in2_zp=[in2; zeros(length(in1)-length(in2),1)];



tmp1=fft(in1);
tmp2=abs(fft(in2_zp));
tmp2=tmp1 .* tmp2;
out=real(ifft(tmp2));
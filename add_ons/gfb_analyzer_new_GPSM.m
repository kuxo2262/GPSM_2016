function analyzer = gfb_analyzer_new_GPSM(fs,flow,basef,fhigh,filters_per_ERBaud,def,gamma_order,bandwidth_factor)
%GFB_ANALYZER_NEW  Construct new analyzer object
%   Usage:  analyzer = gfb_analyzer_new_GPSM(fs,flow, basef, fhigh,filters_per_ERBaud,gamma_order,bandwidth_factor)
%   Input parameters:
%      fs                 : The sampling frequency of the signals on which
%                           the analyzer will operate
%      flow               : The lowest possible center frequency of a
%                           contained gammatone filter
%      basef              : "base frequency". One of the gammatone filters of the analyzer
%                           will have this center frequency.  Must be >= flow
%      fhigh              : The highest possible center frequency of a
%                           contained gammatone filter.  Must be >=  basef
%      filters_per_ERBaud : The density of gammatone filters on the ERB
%                           scale.
%                           NEW features:
%                           'terz': terz distances between gammatone
%                           filters
%                           'octave': octave distances between gammatone
%                           filters
%      gamma_order        : The order of the gammatone filters in this
%                           filterbank. If unspecified, the default value from
%                           gfb_set_constants.m is used.
%      bandwidth_factor   : The bandwidth parameter of the individual filters
%                           is calculated from the Equivalent Rectangular
%                           Bandwidth (ERB) according to equation 14 in
%                           Hohmann (2002). ERB is taken from the Glasberg &
%                           Moore formula for a specific center frequency
%                           (equation 13 in Hohmann (2002)).
%                           Using this parameter, it is possible to widen or
%                           narrow all filters of the filterbank with a
%                           constant bandwidth factor.
%                           Default value is 1.0
%
%   Output parameters:
%      analyzer           : The constructed gfb_analyzer object.
%    
%   `gfb_analyzer_new` constructs a new `gfb_analyzer` object.  The analyzer
%   implements the analysis part of a gammatone filterbank as described
%   in Hohmann (2002).
%
%   It consists of several all-pole gammatone filters; each
%   one with a bandwidth of 1 ERBaud (times bandwidth_factor),
%   and an order of gamma_order.
%
%   The center frequencies of the individual filters are computed as
%   described in section 3 of Hohmann (2002).
%
  
% copyright: Universitaet Oldenburg
% author   : tp
% date     : Jan 2002, Jan, Sep 2003, Nov 2006, Jan 2007

% modified version
%   14/05/2013, 2021-05-21 T. Biberger
% 


if (nargin < 7)
  % The order of the gammatone filter is derived from the global constant
  % GFB_PREFERED_GAMMA_ORDER defined in "gfb_set_constants.m".  Usually,
  % this is equal to 4.
  global GFB_PREFERED_GAMMA_ORDER;
  gfb_set_constants;
  gamma_order = GFB_PREFERED_GAMMA_ORDER;
end
if (nargin < 8)
  bandwidth_factor = 1.0;
end

% To avoid storing information in global variables, we use Matlab
% structures:
analyzer.type                          = 'gfb_analyzer';
analyzer.fs         = fs;
analyzer.flow     = flow;
analyzer.basef = basef;
analyzer.fhigh     = fhigh;
analyzer.filters_per_ERBaud            = filters_per_ERBaud;
analyzer.bandwidth_factor              = bandwidth_factor;
analyzer.fast                          = 0;


if isnumeric(analyzer.filters_per_ERBaud)==0 % so it must be 'terz' or 'octave'
    
    
%     [analyzer.center_frequencies_hz] = terzoctspacebw(flow,fhigh,filters_per_ERBaud,basef);
if strcmp(def.predictions,'SI')==1;
    [analyzer.center_frequencies_hz]=[63    80   100  125  160  200    250    315  400   500  630 800  1000  1250  1600  2000 2500 3150 4000 5000 6300 8000];
elseif strcmp (def.predictions,'PSY')==1;
    [analyzer.center_frequencies_hz]=[63    80   100  125  160  200    250    315  400   500  630 800  1000  1250  1600  2000 2500 3150 4000 5000 6300 8000 10000 12500];
else
    error('Invalid value of def.predictions!')
end
    
elseif isnumeric(analyzer.filters_per_ERBaud)==1 % so it must be somewhat with ERB...
    
    error('This is not a valid auditory filter spacing')
    analyzer.center_frequencies_hz = ...
        erbspacebw(flow,fhigh,1/filters_per_ERBaud,basef);
end



% This loop actually creates the gammatone filters:
for band = [1:length(analyzer.center_frequencies_hz)]
  center_frequency_hz = analyzer.center_frequencies_hz(band);

  % Construct gammatone filter with one ERBaud bandwidth:
  analyzer.filters(1,band) = ...
      gfb_filter_new(fs, center_frequency_hz, ...
                     gamma_order, bandwidth_factor);
end

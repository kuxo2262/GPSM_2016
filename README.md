The Generalized Power Spectrum Model (GPSM; Biberger & Ewert, 2016) requires Signal-Plus-Noise and 
Noise signals as input.

'example_GPSM_simple.m' and 'example_mreGPSM_simple' give minimal examples how to use the long-time GPSM and the mreGPSM for speech intelligibilty experiments in Matlab. 

'example_GPSM_simple_Psy.m' and 'example_mreGPSM_simple_Psy' give minimal examples how to use the long-time GPSM and the mreGPSM for psychoacoustic experiments in Matlab. 

A more detailed description of GPSM and mreGPSM is given in:

T. Biberger, and S. D. Ewert, "Envelope and intensity based prediction of psychoacoustic masking and speech intelligibility",   
Journal of the Acoustical Society of America, vol. 140, no.2, PP.1023-1038. 2016. http://dx.doi.org/10.1121/1.4960574

<h3>Abstract:</h3>
Human auditory perception and speech intelligibility have been successfully described based on the
two concepts of spectral masking and amplitude modulation (AM) masking. The power-spectrum
model (PSM) [Patterson and Moore (1986). Frequency Selectivity in Hearing, pp. 123–177]
accounts for effects of spectral masking and critical bandwidth, while the envelope power-spectrum
model (EPSM) [Ewert and Dau (2000). J. Acoust. Soc. Am. 108, 1181–1196] has been successfully
applied to AM masking and discrimination. Both models extract the long-term (envelope) power to 
calculate signal-to-noise ratios (SNR). Recently, the EPSM has been applied to speech intelligibility 
(SI) considering the short-term envelope SNR on various time scales (multi-resolution speechbased 
envelope power-spectrum model; mr-sEPSM) to account for SI in fluctuating noise
[Jørgensen, Ewert, and Dau (2013). J. Acoust. Soc. Am. 134, 436–446]. Here, a generalized auditory 
model is suggested combining the classical PSM and the mr-sEPSM to jointly account for psychoacoustics 
and speech intelligibility. The model was extended to consider the local AM depth in 
conditions with slowly varying signal levels, and the relative role of long-term and short-term SNR 
was assessed. The suggested generalized power-spectrum model is shown to account for a large 
variety of psychoacoustic data and to predict speech intelligibility in various types of background 
noise.  

<p>&nbsp;</p>

Author of the Matlab implementation of GPSM: 

thomas.biberger@uni-oldenburg.de

<p>&nbsp;</p>

<h2>License and permissions</h2>

Unless otherwise stated, the GPSM distribution, including all files is licensed
under Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
(CC BY-NC-ND 4.0).
In short, this means that you are free to use and share (copy, distribute and
transmit) the GPSM distribution under the following conditions:

Attribution - You must attribute the GPSM distribution by acknowledgement of
              the author if it appears or if was used in any form in your work.
              The attribution must not in any way that suggests that the author
              endorse you or your use of the work.

Noncommercial - You may not use GPSM for commercial purposes.
 
No Derivative Works - You may not alter, transform, or build upon GPSM.

Exceptions are the following external Matlab functions (see their respective licence)
that were used within the GPSM:
- Gammatone filterbank from V. Hohmann (https://zenodo.org/record/2643400#.XQsf5TnVLCM), for details see[1,2]:
   [1] Hohmann, V. (2002). Frequency analysis and synthesis using a Gammatone filterbank. 
       Acta Acustica united with Acustica, 88(3), 433-442.
   [2] Herzke, T., & Hohmann, V. (2007). Improved numerical methods for gammatone filterbank analysis and
       synthesis. Acta acustica united with acustica, 93(3), 498-500. 
- 'MFB2.m' from Stephan D. Ewert and T. Dau
- 'IdealObserver_v1' from Søren Jørgensen

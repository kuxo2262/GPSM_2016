function [out] = mreGPSM(test,noise,fs,def)
% mreGPSM.m Main function for calculation speech intelligibility or
%          psychoacoustics thresholds based on test signal containing the
%          target + noise signals and the noise signal. For detailed
%          information see Biberger and Ewert (2016)
%  
% INPUT:    
%           test   ... target (speech or target modulation) + noise signal
%           noise  ... noise signal
%           fs     ... sampling frequency
%           def    ... struct containing model related infromation (e.g. Psychoac. or SI simulation)
% 
% OUTPUT:   
%           out    ... struct that contains the combined SNR, the envelope
%                      power based SNR and the power based SNR
% 
% 
% Usage: out = mreGPSM(test,noise,fs,def)
% authour: thomas.biberger@uni-oldenburg.de
% update: 2019-01-18, 2021-05-21

%% reorganize same variables
def.samplerate=fs; % sampling freq. in Hz
def.intervallen=length(test);% signal length in samples

%% define parameter/precalculate coefficients
[def simdef simwork]=GPSM_param_def(def);

%% Front end (feature) processing
[test_out work_test dc2mod_test] = mreGPSM_preproc(test,def,simdef,simwork);   % input for the speech signal + noise
[noise_out work_noise dc2mod_noise] = mreGPSM_preproc(noise,def,simdef,simwork);   % input for the noise signal

%% SNR computation and back end processing
[out]=  mreGPSM_feature_out(test_out,noise_out,work_test,simwork,dc2mod_test,def);

end
